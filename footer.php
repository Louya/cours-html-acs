<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;footer&gt;">
    <meta name="keywords" content="HTML,cour,balise,footer">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico"/>
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;footer&gt;</title>
</head>

<body>
    <section class="page">
        <header>
           <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
           <h1>apprenez le html simplement</h1>
       </header>
       <article class="partie_centrale">
            <!-- menu de navigation -->
            <?php $page = 'footer';include("menu.php")?>
            <!-- contenu à droite de la page -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;footer&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256" height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="body.php">&lt;Précédent&gt;</a></p>
                    <p><a href="img.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;footer&gt;</h2>

                    <p>La balise &lt;footer&gt; définit un <strong>pied de page</strong> pour un document ou une section.
                    Un élément &lt;footer&gt; doit contenir des informations sur son élément contenant.
                    <br> Un élément &lt;footer&gt; contient généralement:</p>
                    <ul>
                        <li>Informations sur l'auteur</li>
                        <li>Informations sur le droit d'auteur</li>
                        <li>Informations de contact</li>
                        <li>Plan du site</li>
                        <li>Liens haut de page</li>
                        <li>Documents connexes</li>
                    </ul>
                    <p>Vous pouvez avoir plusieurs éléments &lt;footer&gt; dans un document.</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea rows="15" cols="80">
                                <footer>
                                    <p>copyriht 2018</p>
                                    <p>Contact information:<a href="mailto:robert@example.com">
                                    robert@example.com</a>.</p>
                                </footer>
                        </textarea>
                        <img class="img_exemple" src="media/exemplefooter.jpg" alt="exemple du footer" width="400" height="300">
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <p>La balise &lt;footer&gt; prend également en charge les <a href="https://www.w3schools.com/tags/ref_standardattributes.asp" target=_blank>attributs globaux en HTML</a> 
                </div>
                <!-- partie liens -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <div class="nav_buttons">
                    <p><a href="body.php">&lt;Précédent&gt;</a></p>
                    <p><a href="img.php">&lt;Suivant&gt;</a></p>
                </div>
            </article>
        </article>
    
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>
</body>
</html>
