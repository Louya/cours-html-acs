<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;body&gt;">
    <meta name="keywords" content="HTML,cour,balise,body">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico"/>
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;body&gt;</title>
</head>

<body>
    <section class="page">
        <header>
           <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
           <h1>apprenez le html simplement</h1>
       </header>
       <article class="partie_centrale">
           <!-- menu de navigation -->
           <?php $page = 'body';include("menu.php")?>
           <!-- contenu à droite de la page -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;body&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256" height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="article.php">&lt;Précédent&gt;</a></p>
                    <p><a href="footer.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;body&gt;</h2>
                    <p>Il contient l’ensemble du contenu d’un document HTML comme du texte, des liens hypertexte, des images, des tableaux, des listes…etc.</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                    <textarea>
                        <html>
                            <head>
                            <title>Titre du document</title>
                            </head>
                                    
                            <body>
                            Le contenu du site web....
                            </body>
                                    
                        </html>
                    </textarea>
                    <img class="img_exemple" src="media/exemplebody.png" alt="exemple du body" width="500" height="348" >
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <p>Tous les attributs de mise en page on etait supprimés en HTML5</p>
                </div>
                <!-- partie liens -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <diV class="nav_buttons">
                    <p><a href="article.php">&lt;Précédent&gt;</a></p>
                    <p><a href="footer.php">&lt;Suivant&gt;</a></p>
                </diV>
            </article>
        </article>
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                            
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                    
            </ul>
        </footer>
    </section>
</body>

</html>
