<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;select&gt;">
    <meta name="keywords" content="HTML,cour,balise,select">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico" />
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;select&gt;</title>
</head>

<body>
    <section class="page">
        <header>
            <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
            <h1>apprenez le html simplement</h1>
        </header>
        <article class="partie_centrale">
            <!-- menu de navigation -->
            <?php $page = 'select';include("menu.php")?>
            <!-- partie contenu droite -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;select&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256"
                            height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="nav.php">&lt;Précédent&gt;</a></p>
                    <p><a href="table.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;select&gt;</h2>

                    <p>L'élément &lt;select&gt; est utilisé pour créer une <strong>liste déroulante</strong>.
                        <br>Les balises &lt;option&gt; à l'intérieur de l'élément &lt;select&gt;définissent les options
                        disponibles dans la liste.</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea>
                            <select>
                                <option value="volvo">Volvo</option>
                                <option value="saab">Saab</option>
                                <option value="mercedes">Mercedes</option>
                                <option value="audi">Audi</option>
                            </select>
                        </textarea>
                        <div class="cadre_exemple">
                            <select>
                                <option value="volvo">Volvo</option>
                                <option value="saab">Saab</option>
                                <option value="mercedes">Mercedes</option>
                                <option value="audi">Audi</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <table>
                        <tr class="en-tete">
                            <th>Attribut</th>
                            <th>Valeur</th>
                            <th>Description</th>
                        </tr>
                        <tr class="ligne-paire">
                            <td>autofocus</td>
                            <td>autofocus</td>
                            <td>Indique que la liste déroulante doit automatiquement se concentrer lorsque la page est
                                chargée</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>disabled</td>
                            <td>disabled</td>
                            <td>Indique qu'une liste déroulante doit être désactivée</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>form</td>
                            <td>form id</td>
                            <td>Définit un ou plusieurs formulaires auxquels le champ de sélection appartient</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>multiple</td>
                            <td>multiple</td>
                            <td>Spécifie que plusieurs options peuvent être sélectionnées à la fois</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>name</td>
                            <td>name</td>
                            <td>Définit un nom pour la liste déroulante</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>required</td>
                            <td>required</td>
                            <td>ndique que l'utilisateur doit sélectionner une valeur avant de soumettre le formulaire</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>size</td>
                            <td>number</td>
                            <td>Définit le nombre d'options visibles dans une liste déroulante</td>
                        </tr>
                    </table>
                </div>
                <!-- partie lien -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <div class="nav_buttons">
                    <p><a href="nav.php">&lt;Précédent&gt;</a></p>
                    <p><a href="table.php">&lt;Suivant&gt;</a></p>
                </div>
            </article>
        </article>
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>
</body>

</html>