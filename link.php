<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;link&gt;">
    <meta name="keywords" content="HTML,cour,balise,link">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico"/>
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;link&gt;</title>
</head>

<body>
    <section class="page">
        <header>
           <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
           <h1>apprenez le html simplement</h1>
       </header>
       <article class="partie_centrale">
           <!-- menu de navigation -->
           <?php $page = 'link';include("menu.php")?>
           <!-- contenu à droite de la page -->
            <article class="contenu_principal">
                <div class="titrearticle">
                        <h1>Cours &lt;link&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256" height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="input.php">&lt;Précédent&gt;</a></p>
                    <p><a href="meta.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;link&gt;</h2>
                    <p>La balise &lt;link&gt; définit un <strong>lien</strong> entre un document et une ressource externe.
                    <br>Elle est utilisée pour créer des liens vers des feuilles de style externes.</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea>
                                <head>
                                    <link rel="stylesheet" type="text/css" href="style.css">
                                </head>
                        </textarea>
                        <img class="img_exemple" src="media/exemplelink.PNG" alt="exemple link" width="725" height="285">
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <table>
                        <tr class="en-tete">
                            <th>Attribut</th>
                            <th>Valeur</th>
                            <th>Description</th>
                        </tr>
                        <tr class="ligne-paire">
                            <td>charset</td>
                            <td>charset_set</td>
                            <td>Précise l'encodage des caractères pour le document HTML</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>crossorigin</td>
                            <td>anonymous use-credentials</td>
                            <td>Spécifie comment l'élément gère les requêtes d'origine croisée</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>href</td>
                            <td>URL</td>
                            <td>Spécifie l'emplacement du document lié</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>hreflang</td>
                            <td>language code</td>
                            <td>Spécifie la langue du texte dans le document lié</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>media</td>
                            <td>media query</td>
                            <td>Spécifie sur quel périphérique le document lié sera affiché</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>rel</td>
                            <td>alternate author<br> dns-prefetch help<br> icon license<br> next pingback<br> preconnect prefetch<br> preload prerender<br> prev search<br> stylesheet</td>
                            <td>Champs obligatoires. Spécifie la relation entre le document actuel et le document lié</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>sizes</td>
                            <td>HeightxWidth any</td>
                            <td>Spécifie la taille de la ressource liée. Seulement pour rel = "icon"</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>type</td>
                            <td>media type</td>
                            <td>Spécifie le type de support du document lié</td>
                        </tr>
                    </table>
                </div>
                <!-- partie liens -->
                <div class="cadrearticle2">
                    <h2>lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <div class="nav_buttons">
                        <p><a href="input.php">&lt;Précédent&gt;</a></p>
                        <p><a href="meta.php">&lt;Suivant&gt;</a></p>
                </div>
            </article>
        </article>
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>
</body>

</html>
