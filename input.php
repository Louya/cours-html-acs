<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;input&gt;">
    <meta name="keywords" content="HTML,cour,input">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico"/>
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;input&gt;</title>
</head>


<body>
    <section class="page">
        <header>
           <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
           <h1>apprenez le html simplement</h1>
       </header>
       <article class="partie_centrale">
           <!-- menu de navigation -->
           <?php $page = 'input';include("menu.php")?>
            <!-- contenu à droite de la page -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;input&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256" height="256"></h1>
                </div> 
                <diV class="nav_buttons">
                    <p><a href="img.php">&lt;Précédent&gt;</a></p>
                    <p><a href="link.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;input&gt;</h2>
                    <p>La balise &lt;input&gt; spécifie une zone de saisie ou l'utilisateur peut <strong>entrer des données</strong>.<br>Cet élément s'utilise à l'intérieur d'un élément &lt;form&gt; qui déclare une zone de contrôle de saisie que l'utilisateur peut utiliser pour entrer des données.<br>Un champ de saisi peut fortement varier en fonction de ses <strong>attributs</strong>.</p>
                </div>
                <!-- partie exemple -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea>
                                <form action="/action_page.php">
                                    First name: <input type="text" name="fname"><br>
                                    Last name: <input type="text" name="lname"><br>
                                    <input type="submit" value="Submit">
                                </form>
                        </textarea>
                        <div class="cadre_exemple">
                            <form action="/action_page.php">
                                First name: <input type="text" name="fname"><br>
                                Last name: <input type="text" name="lname"><br>
                                <input type="submit" value="Submit">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">  
                    <h2>Attributs</h2>
                    <table>
                        <tr class="en-tete">
                            <th>Attribut</th>
                            <th>Valeur</th>
                            <th>Description</th>
                        </tr>
                        <tr class="ligne-paire">
                            <td>accept</td>
                            <td>file_extension<br>audio/*<br>video/*<br>image/*<br>media_type</td>
                            <td>Précise le type de fichiers que le serveur accepte</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>align</td>
                            <td>left<br>right<br>top<br>middle<br>bottom<br></td>
                            <td>Non supporté dans html5<br>Précise l'alignement d'une image inséré</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>alt</td>
                            <td>text</td>
                            <td>Précise un texte alternatif pou une image</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>autocomplete</td>
                            <td>on<br>off</td>
                            <td>Précise si un champ &lt;input&gt; doit activer l'autoremplissage ou pas</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>autofocus</td>
                            <td>autofocus</td>
                            <td>Précise qu'un élément &lt;input&gt; doit être automatiquement sélectionné au chargement de la page</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>checked</td>
                            <td>checked</td>
                            <td>Précise qu'un &lt;input&gt; doit être pré-sélectionné au chargement de la page</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>dirname</td>
                            <td>iinputname.dir</td>
                            <td>Précise la direction du text</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>disabled</td>
                            <td>disabled</td>
                            <td>Précise qu'un &lt;input&gt; doit être désactivé</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>form</td>
                            <td>form_id</td>
                            <td>Précise une ou plusieur formes auquel l'élément &lt;input&gt; appartient</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>formaction</td>
                            <td>URL</td>
                            <td>Précise que l'URL du fichier qui va traiter les données saisies dans &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>formenctype</td>
                            <td>application/x-www-form-urlencoded<br>multipart/form-data-text/plain</td>
                            <td>Précise comment les données doivent être encodées quand soumisses au serveur</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>formmethod</td>
                            <td>get<br>post</td>
                            <td>Précise la méthode HTTP utilisé pour envoyer les données</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>formnovalidate</td>
                            <td>formnovalidate</td>
                            <td>Précise que les éléments de forme ne doivent pas être validé quand envoyés</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>formtarget</td>
                            <td>_blanck<br>_self<br>_parent<br>_top<br>framename</td>
                            <td>Précise ou afficher la réponse reçu apres avoir envoyer le formulaire</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>height</td>
                            <td>pixels</td>
                            <td>Précise la hauteur de l' &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>list</td>
                            <td>datalist_id</td>
                            <td>Se réfère à l'élément &lt;datalist&gt; qui cintient des options de pré-remplissage pour l' &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>max</td>
                            <td>number<br>date</td>
                            <td>Précise la valeur maximale de l'élément &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>maxlength</td>
                            <td>number</td>
                            <td>Précise le nombre maximum de caractères autorisé pour l'élément &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>min</td>
                            <td>number<br>date</td>
                            <td>Précise la valeur minimale de l'élément &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>multiple</td>
                            <td>multiple</td>
                            <td>Précise qu"un utilisateur peut entrer plus qu'une valeur dans un &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>name</td>
                            <td>text</td>
                            <td>Précise le nom de l'élément &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>pattern</td>
                            <td>regexp</td>
                            <td>Précise l'expression normale que la valeur d'un &lt;input&gt; est validé à nouveau</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>placeholder</td>
                            <td>text</td>
                            <td>Précise un court indice qui décrit la valeur attendue dans un &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>readonly</td>
                            <td>readonly</td>
                            <td>Précise qu'un &lt;input&gt; peut seulement être lu</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>required</td>
                            <td>required</td>
                            <td>Précise qu'un champ &lt;input&gt; doit être rempli obligatoirement</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>size</td>
                            <td>number</td>
                            <td>Précise la largeur, en caractère,de l'élément &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>src</td>
                            <td>URL</td>
                            <td>Précise l'URL de l'image à utiliser comme boutton de soumission</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>type</td>
                            <td>button<br>checkbox<br>color<br>date<br>datetime-local<br>email<br>file<br>hidden<br>image<br>month<br>number<br>password<br>radio<br>range<br>reset<br>search<br>submit<br>tel<br>text<br>time<br>url<br>week</td>
                            <td>Précise le type d'&lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>value</td>
                            <td>text</td>
                            <td>Précise la valeur de l'élément &lt;input&gt;</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>width</td>
                            <td>pixels</td>
                            <td>Précise la largeur de l'élément &lt;input&gt; (image seulement)</td>
                    </table>
                </div>
                <!-- partie liens utiles -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <div class="nav_buttons">
                        <p><a href="img.php">&lt;Précédent&gt;</a></p>
                        <p><a href="link.php">&lt;Suivant&gt;</a></p>
                </div>
            </article>
        </article>
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>
</body>

</html>
