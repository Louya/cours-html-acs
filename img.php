<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;img&gt;">
    <meta name="keywords" content="HTML,cour,balise,img">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico"/>
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;img&gt;</title>
</head>

<body>
    <section class="page">
        <header>
            <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
            <h1>apprenez le html simplement</h1>
        </header>
        <article class="partie_centrale">
            <!-- menu de navigation -->
            <?php $page = 'img';include("menu.php")?>
            <!-- contenu à droite de la page -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;img&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256" height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="footer.php">&lt;Précédent&gt;</a></p>
                    <p><a href="input.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;img&gt;</h2>
                    <p>La balise &lt;img&gt; définit une <strong>image dans une page html</strong>.<br>
                    La balise &lt;img&gt; a deux attributs obligatoires: src et alt.
                    Remarque: les images ne sont pas techniquement insérées dans une page html, les images sont liées à des pages html.<br> La balise &lt;img&gt; crée un espace de maintien pour l'image référencée.
                    Conseil: Pour lier une image à un autre document, imbriquez simplement la balise &lt;img&gt; dans les balises &lt;a&gt;.</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea>
                            <img src="media/exempleimg.jpg" alt="exemple de l'img" height="640" width="960">
                        </textarea>
                        <img class="img_exemple" src="media/exempleimg.jpg" alt="exemple de l'img" height="640" width="960">
                    </div>
                </div>
                <div class="cadrearticle1">
                    <!-- partie attributs -->
                    <h2>Attributs</h2>
                    <table>
                            <tr class="en-tete">
                                <th>Attribut</th>
                                <th>Valeur</th>
                                <th>Description</th>
                            </tr>
                            <tr class="ligne-paire">
                                <td>alt</td>
                                <td>text</td>
                                <td>Spécifie un autre texte pour une image</td>
                            </tr>
                            <tr class="ligne-impaire">
                                <td>crossorigin</td>
                                <td>anonymous use-credentials</td>
                                <td>Autoriser l'utilisation d'images provenant de sites tiers permettant l'accès à l'origine croisée avec canvas</td>
                            </tr>
                            <tr class="ligne-paire">
                                <td>height</td>
                                <td>pixels</td>
                                <td>Spécifie la hauteur d'une image</td>
                            </tr>
                            <tr class="ligne-impaire">
                                <td>ismap</td>
                                <td>ismap</td>
                                <td>Spécifie une image en tant que carte image côté serveur</td>
                            </tr>
                            <tr class="ligne-paire" >
                                <td>longdesc</td>
                                <td>URL</td>
                                <td>Spécifie une URL pour une description détaillée d'une image</td>
                            </tr>
                            <tr class="ligne-impaire">
                                <td>sizes</td>
                                <td></td>
                                <td>Spécifie les tailles d'image pour différentes mises en page</td>
                            </tr>
                            <tr class="ligne-paire">
                                <td>src</td>
                                <td>URL</td>
                                <td>Spécifie une URL d'une image</td>
                            </tr>
                            <tr class="ligne-impaire" >
                                <td>srcset</td>
                                <td>URL</td>
                                <td>Spécifie l'URL de l'image à utiliser dans différentes situations</td>
                            </tr>
                            <tr class="ligne-paire">
                                <td>usemap</td>
                                <td>#mapname</td>
                                <td>Spécifie une image comme image-map côté client</td>
                            </tr>
                            <tr class="ligne-impaire">
                                <td>width</td>
                                <td>pixels</td>
                                <td>Spécifie la largeur d'une image</td>
                            </tr>
                        </table>
                </div>
                <!-- partie liens -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div> 
                <div class="nav_buttons">
                    <p><a href="footer.php">&lt;Précédent&gt;</a></p>
                    <p><a href="input.php">&lt;Suivant&gt;</a></p>
                </div>
            </article>
        </article>
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>    
</body>