<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;nav&gt;">
    <meta name="keywords" content="HTML,cour,balise,nav">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico" />
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;nav&gt;</title>
</head>

<body>
    <section class="page">
        <header>
            <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
            <h1>apprenez le html simplement</h1>
        </header>
        <article class="partie_centrale">
            <!-- menu de navigation -->
            <?php $page = 'nav';include("menu.php")?>
            <!-- partie contenu droite -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;nav&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256"
                            height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="meta.php">&lt;Précédent&gt;</a></p>
                    <p><a href="select.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;nav&gt;</h2>
                    <p>&lt;nav&gt; est l'une des balises sémantiques html5. Elle représente une section d'une page
                        ayant des liens vers d'autres pages ou des fragments de cette page. Autrement dit, c'est une
                        section destinée à la <strong>navigation</strong> dans un document (avec des menus, des tables
                        des matières, des index, etc.)..</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea rows="15" cols="80">
                            <nav>
                                <a href="/html/">HTML</a> |
                                <a href="/css/">CSS</a> |
                                <a href="/js/">JavaScript</a> |
                                <a href="/jquery/">jQuery</a>
                            </nav>
                        </textarea>
                        <nav>
                            <a href="/html/">HTML</a> |
                            <a href="/css/">CSS</a> |
                            <a href="/js/">JavaScript</a> |
                            <a href="/jquery/">jQuery</a>
                        </nav>
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <p>La balise &lt;nav&gt; prend également en charge les <a href="https://www.w3schools.com/tags/ref_standardattributes.asp"
                            target=_blank>attributs globaux en HTML</a></p>
                </div>
                <!-- partie liens  -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <div class="nav_buttons">
                    <p><a href="meta.php">&lt;Précédent&gt;</a></p>
                    <p><a href="select.php">&lt;Suivant&gt;</a></p>
                </div>
            </article>
        </article>
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>
</body>

</html>