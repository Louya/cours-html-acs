<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;title&gt;">
    <meta name="keywords" content="HTML,cour,balise,title">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico" />
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;title&gt;</title>
</head>

<body>
    <section class="page">
        <header>
            <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
            <h1>apprenez le html simplement</h1>
        </header>
        <article class="partie_centrale">
            <!-- menu de navigation -->
            <?php $page = 'title';include("menu.php")?>
            <!-- partie contenu de droite -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;title&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256"
                            height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="textarea.php">&lt;Précédent&gt;</a></p>
                    <p><a href="index.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;title&gt;</h2>
                    <p>La balise &lt;title&gt; est requise dans tous les documents HTML et définit le titre du
                        document.</p>
                    <p>Il définit un titre dans la barre d’outils du navigateur, fournit un titre lorsque la page est
                        ajoutée
                        aux favoris et affiche également un titre pour la page dans les résultats de la recherche.</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea rows="15" cols="80">
                            <!DOCTYPE html>
                            <html>
                                
                            <head>
                            <title>Document</title>
                            </head>
                                
                            <body>
                                le contenue du site web......
                            </body>
                                
                            </html>
                        </textarea>

                        <img src="media/title.png" alt="exemple de la balise title" width="389" height="127">
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <p>La balise &lt;title&gt; ne possede pas d'attributs</p>
                </div>
                <!-- partie liens -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <div class="nav_buttons">
                    <a href="textarea.php">&lt;Précédent&gt;</a>
                    <a href="index.php">&lt;Suivant&gt;</a>
                </div>
            </article>
        </article>
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>
</body>

</html>