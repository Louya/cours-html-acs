<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;textarea&gt;">
    <meta name="keywords" content="HTML,cour,balise,textarea">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico" />
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;textea&gt;</title>
</head>

<body>
    <section class="page">
        <header>
            <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
            <h1>apprenez le html simplement</h1>
        </header>
        <article class="partie_centrale">
            <!-- menu de navigation -->
            <?php $page = 'textarea';include("menu.php")?>
            <!-- partie contenu droite -->
            <article>
                <div class="titrearticle">
                    <h1>Cours &lt;textarea&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256"
                            height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="table.php">&lt;Précédent&gt;</a></p>
                    <p><a href="title.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;textarea&gt;</h2>

                    <p>La balise &lt;textarea&gt; définit un contrôle de saisie de texte sur plusieurs
                        lignes,&lt;textarea&gt; est tout simplement une <strong>zone de texte</strong>.
                        <br> Une zone de texte peut contenir un nombre illimité de caractères et le texte est rendu
                        dans une police à largeur fixe (généralement Courier).
                        <br> La taille d'une zone de texte peut être spécifiée par les attributs cols et rows, ou même
                        mieux. grâce aux propriétés de hauteur et de largeur CSS
                    </p>
                </div>
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea rows="15" cols="80">
                <textarea rows="4" cols="50">
                    ceci est une zone de texte 
                     &lt;/textarea&gt;
                </textarea>

                    <textarea rows="4" cols="50">
                    ceci est une zone de texte 
            </textarea>
        </div>
    </div>
    <div class="cadrearticle1">
        <h2>Attributs</h2>
        <table>
            <tr class="en-tete">
                <th>Attribut</th>
                <th>Valeur</th>
                <th>Description</th>
            </tr>
            <tr class="ligne-paire">
                <td>autofocus</td>
                <td>autofocus</td>
                <td>Spécifie qu'une zone de texte doit automatiquement se concentrer lorsque la page est chargée</td>
            </tr>
            <tr class="ligne-impaire">
                <td>cols</td>
                <td>number</td>
                <td>Spécifie la largeur visible d'une zone de texte</td>
            </tr>
            <tr class="ligne-paire">
                <td>dirname</td>
                <td>textareaname.dir</td>
                <td>Indique que la direction du texte de textarea sera soumise</td>
            </tr>
            <tr class="ligne-impaire">
                <td>disabled</td>
                <td>disabled</td>
                <td>Spécifie qu'une zone de texte doit être désactivée</td>
            </tr>
            <tr class="ligne-paire">
                <td>form</td>
                <td>form id</td>
                <td>Spécifie un ou plusieurs formulaires auxquels la zone de texte appartient</td>
            </tr>
            <tr class="ligne-impaire">
                <td>maxlength</td>
                <td>number</td>
                <td>Spécifie le nombre maximum de caractères autorisés dans la zone de texte</td>
            </tr>
            <tr class="ligne-paire">
                <td>name</td>
                <td>text</td>
                <td>Spécifie un nom pour une zone de texte</td>
            </tr>
            <tr class="ligne-impaire">
                <td>placeholder</td>
                <td>text</td>
                <td>Spécifie un court indicateur décrivant la valeur attendue d'une zone de texte</td>
            </tr>
            <tr class="ligne-paire">
                <td>readonly</td>
                <td>readonly</td>
                <td>Indique qu'une zone de texte doit être en lecture seule</td>
            </tr>
            <tr class="ligne-impaire">
                <td>required</td>
                <td>required</td>
                <td>Indique qu'une zone de texte est requise / doit être remplie</td>
            </tr>
            <tr class="ligne-paire">
                <td>rows</td>
                <td>number</td>
                <td>Spécifie le nombre visible de lignes dans une zone de texte</td>
            </tr>
            <tr class="ligne-impaire">
                <td>wrap</td>
                <td>hard soft</td>
                <td>Spécifie comment le texte dans une zone de texte doit être encapsulé lorsqu'il est soumis dans un formulaire</td>
            </tr>
        </table>
    </div>
    </div>
    </div>
    <div class="cadrearticle2">
        <h2>Lien utile</h2>
        <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
    </div>
    <div class="nav_buttons">
        <p><a href="table.php">&lt;Précédent&gt;</a></p>
        <p><a href="title.php">&lt;Suivant&gt;</a></p>
    </div>
    </article>
</article>
    <footer>
        <p>© 2018</p>  
        <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
        <ul class="ulfooter">
                      
            <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
            <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
            <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
              
        </ul>
    </footer>
</section>
</body>
</html>