<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;table&gt;">
    <meta name="keywords" content="HTML,cour,balise,table">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico" />
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;table&gt;</title>
</head>

<body>
    <section class="page">
        <header>
            <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
            <h1>apprenez le html simplement</h1>
        </header>
        <article class="partie_centrale">
            <!-- menu de navigation -->
            <?php $page = 'table';include("menu.php")?>
            <!-- partie contenu droite -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;table&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256"
                            height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="select.php">&lt;Précédent&gt;</a></p>
                    <p><a href="textarea.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                    <h2>Definition et utilisation de la balise &lt;table&gt;</h2>
                    <p>La balise &lt;table&gt; défini un <strong>tableau HTML</strong>.<br>Un tableau HTML est
                        constitué d'un ou plusieurs éléments &lt;tr&gt;,&lt;th&gt; et &lt;td&gt;.<br>La balise
                        &lt;tr&gt; précise les <strong>lignes</strong> du tableau, la balise &lt;th&gt; indique <strong>l'entête</strong>
                        du tableau et la balise &lt;td&gt; précise une <strong>cellule</strong> du tableau.<br>On peut
                        compléxifier un tableau à l'aide de nombreux attributs.</p>
                </div>
                <!-- partie exemples -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea rows="15" cols="80">
                            <table>
                                <tr>
                                <th>Month</th>
                                <th>Savings</th>
                                </tr>
                                <tr>
                                <td>January</td> 
                                <td>$100</td>
                                </tr>
                            </table>
                        </textarea>
                        <div class="cadre_exemple">
                            <table>
                                <tr>
                                    <th>Month</th>
                                    <th>Savings</th>
                                </tr>
                                <tr>
                                    <td>January</td>
                                    <td>$100</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- partie attributs -->
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <p>La balise &lt;table&gt; prend également en charge les <a href="https://www.w3schools.com/tags/ref_standardattributes.asp"
                            target=_blank>attributs globaux en HTML</a></p>
                </div>
                <!-- partie lien -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <diV class="nav_buttons">
                    <p><a href="select.php">&lt;Précédent&gt;</a></p>
                    <p><a href="textarea.php">&lt;Suivant&gt;</a></p>
                </diV>
            </article>
        </article>           
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                              
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                      
            </ul>
        </footer>
    </section>
</body>

</html>