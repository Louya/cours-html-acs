<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cour sur la balise &lt;meta&gt;">
    <meta name="keywords" content="HTML,cour,balise,meta">
    <meta name="author" content="Fabien Cazalet, Alexandre Repis, Aurélien Deloye">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="media/favicon.ico"/>
    <link rel="stylesheet" href="style.css">
    <title>Cours &lt;meta&gt;</title>
 </head>   

<body>
    <section class="page">
        <header>
           <a href="index.html"><img src="media/logocourshtml.png" alt="Cours HTML" width="542" height="90"></a>
           <h1>apprenez le html simplement</h1>
       </header>
       <article class="partie_centrale">
           <!-- menu de navigation -->
           <?php $page = 'meta';include("menu.php")?>
           <!-- contenu à droite de la page -->
            <article class="contenu_principal">
                <div class="titrearticle">
                    <h1>Cours &lt;meta&gt; <img class="html" src="media/logo_html5_256.png" alt="logo html5" width="256" height="256"></h1>
                </div>
                <diV class="nav_buttons">
                    <p><a href="link.php">&lt;Précédent&gt;</a></p>
                    <p><a href="nav.php">&lt;Suivant&gt;</a></p>
                </diV>
                <div class="cadrearticle1">
                <h2>Definition et utilisation de la balise &lt;meta&gt;</h2>
                <p>Une métadonnée est une donné(information) à propos d'une information.
                    <br>L'élément HTML &lt;meta&gt; fournis les <strong>métadonnées</strong> à propos du document HTML. Ces métadonnées ne seront pas affichés sur la page, mais le navigateur les lira.
                    <br>Les méta éléments sont typiquement utilisé pour spécifier la description d'une page, les mots clés, etc..
                    <br>Elles sont utilisé parle navigateur(comment afficher le contenu ou recharger la page) ou par les moteurs de recherches (mots clés) ou d'autres services web.</p>
                </div>
                <!-- exemple d'application -->
                <div class="cadrearticle2">
                    <h2>Exemple pratique</h2>
                    <div class="exemple">
                        <textarea rows="15" cols="80">
                            <head>
                                <meta charset="UTF-8">
                                <meta name="description" content="Free Web tutorials">
                                <meta name="keywords" content="HTML,CSS,XML,JavaScript">
                                <meta name="author" content="John Doe">
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            </head>
                        </textarea>
                        <img class="img_exemple" src="media/exemplemeta.PNG " alt="exemple de la balise meta" width="980" height="260">
                    </div>
                </div>
                <div class="cadrearticle1">
                    <h2>Attributs</h2>
                    <!--tableau des attributs-->
                    <table>
                        <tr class="en-tete">
                            <th>Attribut</th>
                            <th>Valeur</th>
                            <th>Description</th>
                        </tr>
                        <tr class="ligne-paire">
                            <td>charset</td>
                            <td>charset_set</td>
                            <td>Précise l'encodage des caractères pour le document HTML</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>content</td>
                            <td>text</td>
                            <td>Précise la valeur associé avec le http-equiv ou le nom d'attibut</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>http-equiv</td>
                            <td>content-type<br>default-style<br>refresh</td>
                            <td>Fourni un header HTTP pour les informations/valeurs de l'attribut</td>
                        </tr>
                        <tr class="ligne-impaire">
                            <td>name</td>
                            <td>application-name<br>author<br>description<br>generator<br>keywords<br>viewport
                            </td>
                            <td>Précise le nom des métadonnées</td>
                        </tr>
                        <tr class="ligne-paire">
                            <td>scheme</td>
                            <td>forat/URI</td>
                            <td>Pas supporté par HTML5<br>Précise un schema à utiliser pour interpréter la valeur du contenu de l'attribut</td>
                        </tr>
                    </table>
                </div>
                <!-- partie liens -->
                <div class="cadrearticle2">
                    <h2>Lien utile</h2>
                    <a class="lien" href="https://www.w3schools.com" target="_blank">w3schools.com</a>
                </div>
                <div class="nav_buttons">
                    <p><a href="link.php">&lt;Précédent&gt;</a></p>
                    <p><a href="nav.php">&lt;Suivant&gt;</a></p>
                </div>
            </article>
        </article>   
        <footer>
            <p>© 2018</p>  
            <a href="https://www.facebook.com/COURS-HTML-538352666611071/?modal=admin_todo_tour" target="_blank"><img id="facebook" src="media/icon_facebook.png" alt="logo facebook" width="58" height="58"></a>                 
            <ul class="ulfooter">
                        
                <li><a class="mail" href="mailto:alexandre.r@codeur.online">alexandre.r@codeur.online</a></li>
                <li><a class="mail" href="mailto:aurelien.d@codeur.online">aurelien.d@codeur.online</a></li>
                <li><a class="mail" href="mailto:fabien.c@codeur.online">fabien.c@codeur.online</a></li>
                
            </ul>
        </footer>
    </section>
</body>

</html>
